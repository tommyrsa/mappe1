package com.github.henduww.mappe1.hospital;

import com.github.henduww.mappe1.hospital.healthpersonnel.Employee;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Administrative department functionality")
public class DepartmentTest
{
    private final Department dpt = new Department("Avd. eling 7");

    @Nested
    @DisplayName("Patients and employees are removable from registry")
    class peopleAreRemovable
    {
        @Test
        @DisplayName("Patient registered in Department is removeable")
        public void patientIsRemoveable()
        {
            Patient patient = new Patient("Pasi", "Enten", "22030588452");

            // Patient must first be added in ordered to be removed
            dpt.addPatient(patient);

            // Patient should be successfully removed
            try
            {
                dpt.remove(patient);
            }
            catch (RemoveException e)
            {
                assertNotEquals(e.getMessage(), "Patient not found.");
            }
        }

        @Test
        @DisplayName("Employee registered in Department is removeable")
        public void employeeIsRemoveable()
        {
            Employee employee = new Employee("Ansa", "Tte", "22030588452");

            // Employee must first be added in ordered to be removed
            dpt.addEmployee(employee);

            // Employee should be successfully removed
            try
            {
                dpt.remove(employee);
            }
            catch (RemoveException e)
            {
                assertNotEquals(e.getMessage(), "Employee not found.");
            }
        }
    }

    @Nested
    class peopleAreNotRemoveable
    {
        @Test
        @DisplayName("Patient not registered in Department is not removeable")
        public void patientIsNotRemoveable()
        {
            Patient patient = new Patient("Pasi", "Enten", "22030588452");

            // Patient must first be added in ordered to be removed
            dpt.addPatient(patient);

            // Trying to remove non-existing patient
            try
            {
                dpt.remove(new Patient("Noe", "Annet", "05079978542"));
            }
            catch (RemoveException e)
            {
                assertEquals(e.getMessage(), "Patient not found.");
            }
        }

        @Test
        @DisplayName("Employee not registered in Department is not removeable")
        public void employeeIsNotRemoveable()
        {
            Employee employee = new Employee("Ansa", "Tte", "22030588452");

            // Employee must first be added in ordered to be removed
            dpt.addEmployee(employee);

            // Trying to remove non-existing employee
            try
            {
                dpt.remove(new Employee("Noe", "Annet", "05079978542"));
            }
            catch (RemoveException e)
            {
                assertEquals(e.getMessage(), "Employee not found.");
            }
        }
    }
}
