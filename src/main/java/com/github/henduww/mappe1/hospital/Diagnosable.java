package com.github.henduww.mappe1.hospital;

/**
 * Defines a Diagnosable interface.
 * Should be implemented by any classes that may be diagnosed.
 */
public interface Diagnosable {
    /**
     * Sets diagnosis of the Diagnosable.
     * @param diagnosis Diagnosis to set.
     */
    void setDiagnosis(String diagnosis);
}
