package com.github.henduww.mappe1.hospital;

/**
 * Describes a patient.
 * Defined as a person due to its heritage.
 * Implements the Diagnosable interface to allow doctors to set diagnoses.
 */
public class Patient extends Person implements Diagnosable
{
    private String diagnosis;

    public Patient(String firstName, String lastName, String socialSecurityNumber)
    {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * @return Diagnose given to patient.
     */
    public String getDiagnose()
    {
        return this.diagnosis;
    }

    @Override
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
}
