package com.github.henduww.mappe1.hospital;

public abstract class Person
{
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    public Person(String firstName, String lastName, String socialSecurityNumber)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstName()
    {
        return this.firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return this.lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getSocialSecurityNumber()
    {
        return this.socialSecurityNumber;
    }

    public String getFullName()
    {
        return this.firstName + " " + this.lastName;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber)
    {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof Person)
        {
            Person person = (Person) obj;
            return this.getFullName().equals(person.getFullName()) && this.socialSecurityNumber.equals(person.getSocialSecurityNumber());
        }

        return false;
    }

    @Override
    public String toString()
    {
        return this.getFullName();
    }
}
