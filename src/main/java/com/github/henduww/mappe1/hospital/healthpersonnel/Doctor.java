package com.github.henduww.mappe1.hospital.healthpersonnel;

import com.github.henduww.mappe1.hospital.Patient;

/**
 * Describes an abstract Doctor class, capable of diagnosing patients.
 * Defined as an employee by its heritage.
 */
public abstract class Doctor extends Employee
{
    public Doctor(String firstName, String lastName, String socialSecurityNumber)
    {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Sets diagnosis for Patient. Should be implemented by child classes.
     * @param patient Patient to diagnose.
     * @param diagnosis Diagnosis for patient.
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
