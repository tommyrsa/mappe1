package com.github.henduww.mappe1.hospital;

import com.github.henduww.mappe1.hospital.healthpersonnel.Employee;

import java.util.ArrayList;
import java.util.List;

/**
 * Describes a hospital department, providing functionality required for administrating one.
 */
public class Department
{
    private String name;
    private final List<Employee> employees;
    private final List<Patient> patients;
    
    public Department(String name)
    {
        this.name = name;
        employees = new ArrayList<>();
        patients = new ArrayList<>();
    }

    /**
     * @return Department name.
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * Sets new name for department.
     * @param name New department name.
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return A list of all registered employees within the department.
     */
    public List<Employee> getEmployees()
    {
        return this.employees;
    }

    /**
     * Adds an employee to the department register.
     * @param employee New employee.
     */
    public void addEmployee(Employee employee)
    {
        this.employees.add(employee);
    }

    /**
     * @return A list of all registered patients within the department.
     */
    public List<Patient> getPatients()
    {
        return this.patients;
    }

    /**
     * Adds a patient to the department register.
     * @param patient New patient.
     */
    public void addPatient(Patient patient)
    {
        this.patients.add(patient);
    }

    /**
     * Removes a person from the department register.
     * @param person Person to remove.
     * @throws RemoveException If Person was not found within any department register.
     */
    public void remove(Person person) throws RemoveException
    {
        if (person instanceof Employee)
        {
            if (this.employees.contains(person))
            {
                this.employees.remove(person);
            }
            else
            {
                throw new RemoveException("Employee not found.");
            }
        }
        else if (person instanceof Patient)
        {
            if (this.patients.contains(person))
            {
                this.patients.remove(person);
            }
            else
            {
                throw new RemoveException("Patient not found.");
            }
        }
    }

    /**
     * Override of `Object`'s equals method. Used for comparing departments.
     * @param obj Object to compare.
     * @return Whether or not the objects are the same.
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof Department)
        {
            Department dpt = (Department) obj;
            return this.name.equals(dpt.getName());
        }

        return false;
    }

    /**
     * Override of `Object`'s toString method.
     * @return Department name.
     */
    @Override
    public String toString()
    {
        return this.name;
    }
}
