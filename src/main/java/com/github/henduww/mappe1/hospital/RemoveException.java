package com.github.henduww.mappe1.hospital;

/**
 * Exception thrown when attempting to remove a non-existing element in a list.
 */
public class RemoveException extends Exception {
    public RemoveException(String errorMessage)
    {
        super(errorMessage);
    }
}
