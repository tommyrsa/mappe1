package com.github.henduww.mappe1.hospital.healthpersonnel;

import com.github.henduww.mappe1.hospital.Person;

/**
 * Describes a hospital employee.
 */
public class Employee extends Person
{
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
}
