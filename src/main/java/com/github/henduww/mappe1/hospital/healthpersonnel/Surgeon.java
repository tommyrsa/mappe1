package com.github.henduww.mappe1.hospital.healthpersonnel;

import com.github.henduww.mappe1.hospital.Patient;
/**
 * Describes a surgeon.
 * Defined as a Doctor due to its heritage.
 */
public class Surgeon extends Doctor
{
    public Surgeon(String firstName, String lastName, String socialSecurityNumber)
    {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public void setDiagnosis(Patient patient, String diagnosis)
    {
        patient.setDiagnosis(diagnosis);
    }
}
