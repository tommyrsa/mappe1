package com.github.henduww.mappe1.hospital.healthpersonnel;

/**
 * Describes a nurse.
 * Defined as an employee due to its heritage.
 */
public class Nurse extends Employee {
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
}
