package com.github.henduww.mappe1.hospital.healthpersonnel;

import com.github.henduww.mappe1.hospital.Patient;

/**
 * Describes a general practitioner.
 * Defined as a Doctor due to its heritage.
 */
public class GeneralPractitioner extends Doctor
{
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber)
    {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public void setDiagnosis(Patient patient, String diagnosis)
    {
        patient.setDiagnosis(diagnosis);
    }
}
