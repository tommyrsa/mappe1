package com.github.henduww.mappe1.hospital;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a hospital.
 */
public class Hospital
{
    private final String name;
    private final List<Department> departments;

    public Hospital(String name)
    {
        this.name = name;
        departments = new ArrayList<>();
    }

    /**
     * @return Name of the hospital.
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * @return A list of departments associated with the hospital.
     */
    public List<Department> getDepartments()
    {
        return this.departments;
    }

    /**
     * Adds a department to be associated to the hospital.
     * @param department Department to associate with the hospital.
     */
    public void addDepartment(Department department)
    {
        departments.add(department);
    }

    /**
     * Override of `Object`'s toString method.
     * @return Name of the hospital.
     */
    @Override
    public String toString()
    {
        return this.name;
    }
}
