package com.github.henduww.mappe1.client;

import com.github.henduww.mappe1.hospital.Department;
import com.github.henduww.mappe1.hospital.Hospital;
import com.github.henduww.mappe1.hospital.RemoveException;
import com.github.henduww.mappe1.hospital.healthpersonnel.Employee;

public class HospitalClient {
    private static Hospital hospital;

    /**
     * Test client as described in assignment 5.
     * @param args Command line arguments.
     */
    public static void main(String[] args)
    {
        hospital = new Hospital("Jawa Medical Center");

        HospitalTestData.fillRegisterWithTestData(hospital);

        Department dpt = hospital.getDepartments().get(0);
        Employee toRemove = new Employee("Odd Even", "Primtallet", "");

        // Remove existing employee, as described in assignment
        try {
            dpt.remove(toRemove);
            System.out.println("Successfully removed " + toRemove);
        }
        catch (RemoveException e)
        {
            System.out.println("Error occured: " + e.getMessage() + "\nWhen trying to remove " + toRemove);
        }

        toRemove = new Employee("Finnes", "ikke", "");

        // Attempt removal of non-existing employee, as described in assignment
        try {
            dpt.remove(toRemove);
            System.out.println("Successfully removed " + toRemove);
        }
        catch (RemoveException e)
        {
            System.out.println("Error occured: " + e.getMessage() + "\nWhen trying to remove " + toRemove);
        }
    }
}
